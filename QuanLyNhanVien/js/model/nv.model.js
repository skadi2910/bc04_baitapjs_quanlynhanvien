class NhanVien {
  constructor(
    accountId,
    name,
    email,
    password,
    startedDate,
    wages,
    position,
    workingHour
  ) {
    this.accountId = accountId;
    this.name = name;
    this.email = email;
    this.password = password;
    this.startedDate = startedDate;
    this.wages = wages;
    this.position = position;
    this.workingHour = workingHour;

    this.getSalary = function () {
      switch (this.position) {
        case "Sếp": {
          return this.wages * 3;
        }
        case "Trưởng phòng": {
          return this.wages * 2;
        }
        case "Nhân viên": {
          return this.wages * 1;
        }
        default:
          return this.wages * 1;
      }
    };

    this.classifyNhanVien = function () {
      let workingHour = this.workingHour * 1;
      if (workingHour >= 192 || this.position === "Sếp") {
        return "Xuất Sắc";
      } else if (workingHour >= 176) {
        return "Tốt";
      } else if (workingHour >= 160) {
        return "Khá";
      } else {
        return "Trung Bình";
      }
    };
  }
}
