const validator = {
  checkEmptyString: function (value, errorId, message) {
    if (value.length == 0) {
      document.querySelector(errorId).innerText = message;
      return false;
    } else {
      document.querySelector(errorId).innerText = "";
      return true;
    }
  },
  checkRange: function (value, min, max, errorId, message) {
    if (value < min || value > max) {
      document.querySelector(errorId).innerText = message;
      return false;
    } else {
      document.querySelector(errorId).innerText = "";
      return true;
    }
  },
  checkEmail: function (value, errorId, message) {
    const regexEmail =
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (regexEmail.test(value)) {
      document.querySelector(errorId).innerText = "";
      return true;
    } else {
      document.querySelector(errorId).innerText = message;
      return false;
    }
  },
  checkPassword: function (value, errorId, message) {
    const regexPassword =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&+~|{}:;<>/])[A-Za-z\d$@$!%*?&+~|{}:;<>/]*$/;
    if (regexPassword.test(value)) {
      document.querySelector(errorId).innerText = "";
      return true;
    } else {
      document.querySelector(errorId).innerText = message;
      return false;
    }
  },
  checkNumber: function (value, errorId, message) {
    const regexNumber = /^[0-9]*$/;
    if (regexNumber.test(value)) {
      document.querySelector(errorId).innerText = "";
      return true;
    } else {
      document.querySelector(errorId).innerText = message;
      return false;
    }
  },
  checkAlphabetLetter: function (value, errorId, message) {
    const regexLetter = /^[A-Za-z ]*$/;
    if (regexLetter.test(value)) {
      document.querySelector(errorId).innerText = "";
      return true;
    } else {
      document.querySelector(errorId).innerText = message;
      return false;
    }
  },
  checkDateFormat: function (value, errorId, message) {
    const [month, day, year] = value.split("/");
    const dateFormatRegex =
      /^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d$/;

    function isLeapYear(year) {
      let _year = year * 1;
      if (_year % 100 == 0) {
        if (_year % 400 == 0) {
          return true;
        } else {
          return false;
        }
      } else if (_year % 4 == 0) {
        return true;
      } else {
        return false;
      }
    }
    if (dateFormatRegex.test(value)) {
      if (
        day == 31 &&
        (month == 4 || month == 6 || month == 9 || month == 11)
      ) {
        document.querySelector("#tbNgay").innerText = "Wrong date";
        return false;
      } else if (
        day == 30 &&
        (month == 1 ||
          month == 3 ||
          month == 5 ||
          month == 7 ||
          month == 8 ||
          month == 10 ||
          month == 12)
      ) {
        document.querySelector("#tbNgay").innerText = "Wrong date";
        return false;
      } else if (day >= 30 && month == 2) {
        document.querySelector("#tbNgay").innerText = "Wrong date";
        return false;
      } else if (day == 29 && month == 2 && isLeapYear(year) == false) {
        document.querySelector("#tbNgay").innerText = "Wrong date";
        return false;
      } else {
        document.querySelector(errorId).innerText = "";
        return true;
      }
    } else {
      document.querySelector(errorId).innerText = message;
      return false;
    }
  },
};
const checkDuplicated = {
  id: function (value, nhanVienList, duplicatedLimit, errorId, message) {
    let count = 0;
    for (index = 0; index < nhanVienList.length; index++) {
      let nhanVien = nhanVienList[index];
      if (nhanVien.accountId == value) {
        count++;
      }
    }
    if (count < duplicatedLimit) {
      return true;
    } else {
      document.querySelector(errorId).innerText = message;
      return false;
    }
  },
  email: function (value, nhanVienList, duplicatedLimit, errorId, message) {
    let count = 0;
    for (index = 0; index < nhanVienList.length; index++) {
      let nhanVien = nhanVienList[index];
      if (nhanVien.email == value) {
        count++;
      }
    }
    if (count < duplicatedLimit) {
      return true;
    } else {
      document.querySelector(errorId).innerText = message;
      return false;
    }
  },
};
