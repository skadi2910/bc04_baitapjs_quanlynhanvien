function getValuefromForm() {
  const accountId = document.querySelector("#tknv").value;
  const name = document.querySelector("#name").value;
  const email = document.querySelector("#email").value;
  const password = document.querySelector("#password").value;
  const startedDate = document.querySelector("#datepicker").value;
  const wages = document.querySelector("#luongCB").value;
  const position = document.querySelector("#chucvu").value;

  const workingHour = document.querySelector("#gioLam").value;

  return new NhanVien(
    accountId,
    name,
    email,
    password,
    startedDate,
    wages,
    position,
    workingHour
  );
}
function showValueToForm(nhanVien) {
  document.querySelector("#tknv").readOnly = true;
  document.querySelector("#tknv").value = nhanVien.accountId;
  document.querySelector("#name").value = nhanVien.name;
  document.querySelector("#email").value = nhanVien.email;
  document.querySelector("#password").value = nhanVien.password;
  document.querySelector("#datepicker").value = nhanVien.startedDate;
  document.querySelector("#luongCB").value = nhanVien.wages;
  document.querySelector("#chucvu").value = nhanVien.position;
  document.querySelector("#gioLam").value = nhanVien.workingHour;
}
function renderNhanVienList(nhanVienList) {
  let contentHtml = "";
  for (let index = 0; index < nhanVienList.length; index++) {
    let nhanVien = nhanVienList[index];
    let contentTr = `
    <tr>
        <td>${nhanVien.accountId}</td>
        <td>${nhanVien.name}</td>
        <td>${nhanVien.email}</td>
        <td>${nhanVien.startedDate}</td>
        <td>${nhanVien.position}</td>
        <td>${new Intl.NumberFormat().format(nhanVien.getSalary())}</td>
        <td>${nhanVien.classifyNhanVien()}</td>
        <td>
            <i role="button" class="fa fa-edit mr-2" style="color: green; cursor: pointer" onClick="editNhanVien(${
              nhanVien.accountId
            })" data-toggle="modal" data-target="#myModal"></i>
            <i role="button" class="fa fa-trash-alt " style="color: red; cursor: pointer" onClick="deleteNhanVien(${
              nhanVien.accountId
            })"></i>
        </td>  
    </tr>`;
    contentHtml += contentTr;
  }
  document.querySelector("#tableDanhSach").innerHTML = contentHtml;
}
function validateInput(nhanVien) {
  const isValidId =
    validator.checkEmptyString(
      nhanVien.accountId,
      "#tbTKNV",
      "Must not be empty"
    ) &&
    validator.checkRange(
      nhanVien.accountId.length,
      1,
      6,
      "#tbTKNV",
      "Must be 1-6 digit"
    ) &&
    validator.checkNumber(nhanVien.accountId, "#tbTKNV", "Must be number");

  const isValidName =
    validator.checkEmptyString(nhanVien.name, "#tbTen", "Must not be empty") &&
    validator.checkAlphabetLetter(
      nhanVien.name,
      "#tbTen",
      "Must be Alphabet letters"
    );

  const isValidEmail =
    validator.checkEmptyString(
      nhanVien.email,
      "#tbEmail",
      "Must not be empty"
    ) &&
    validator.checkEmail(nhanVien.email, "#tbEmail", "Invalid email format");

  const isValidPassword =
    validator.checkEmptyString(
      nhanVien.password,
      "#tbMatKhau",
      "Must not be empty"
    ) &&
    validator.checkRange(
      nhanVien.password.length,
      6,
      10,
      "#tbMatKhau",
      "Must be 6-10 digit"
    ) &&
    validator.checkPassword(
      nhanVien.password,
      "#tbMatKhau",
      "Must include: 1 Uppercase, 1 Lowercase, 1 Number and 1 Special character"
    );

  const isValidDate =
    validator.checkEmptyString(
      nhanVien.startedDate,
      "#tbNgay",
      "Must not be empty"
    ) &&
    validator.checkDateFormat(
      nhanVien.startedDate,
      "#tbNgay",
      "Must be MM/DD/YYYY"
    );

  const isValidWages =
    validator.checkEmptyString(
      nhanVien.wages,
      "#tbLuongCB",
      "Must not be empty"
    ) &&
    validator.checkNumber(nhanVien.wages, "#tbLuongCB", "Must be number") &&
    validator.checkRange(
      nhanVien.wages * 1,
      1000000,
      20000000,
      "#tbLuongCB",
      "Must in range: 1.000.000 - 20.000.000"
    );

  const isValidPosition = validator.checkEmptyString(
    nhanVien.position,
    "#tbChucVu",
    "Must be valid"
  );

  const isValidWorkingHour =
    validator.checkEmptyString(
      nhanVien.workingHour,
      "#tbGiolam",
      "Must not be empty"
    ) &&
    validator.checkNumber(
      nhanVien.workingHour,
      "#tbGiolam",
      "Must be number"
    ) &&
    validator.checkRange(
      nhanVien.workingHour * 1,
      80,
      200,
      "#tbGiolam",
      "Must in range 80 - 200"
    );

  return (
    isValidId &&
    isValidName &&
    isValidEmail &&
    isValidPassword &&
    isValidDate &&
    isValidWages &&
    isValidPosition &&
    isValidWorkingHour
  );
}

function checkDuplicatedAdd(nhanVien, nhanVienList) {
  const isDuplicatedId = checkDuplicated.id(
    nhanVien.accountId,
    nhanVienList,
    1,
    "#tbTKNV",
    "ID Already Exist"
  );
  const isDuplicatedEmail = checkDuplicated.email(
    nhanVien.email,
    nhanVienList,
    1,
    "#tbEmail",
    "Email Already Exist"
  );
  return isDuplicatedId && isDuplicatedEmail;
}

function checkDuplicatedUpdate(nhanVien, nhanVienList) {
  const isDuplicatedEmail = checkDuplicated.email(
    nhanVien.email,
    nhanVienList,
    2,
    "#tbEmail",
    "Email Already Exist"
  );
  return isDuplicatedEmail;
}
