// GLOBAL VARIABLES
let nhanVienList = [];
let editId = "";
let editIndex = "";
const alertElement = document.querySelector(".alert");
const addNvBtn = document.querySelector("#btnThemNV");
const updateNvBtn = document.querySelector("#btnCapNhat");
const closeBtn = document.querySelector("#btnDong");
const searchBtn = document.querySelector("#btnTimNV");
const clearSearchBtn = document.querySelector("#btnXoaTim");
const sapXepTangBtn = document.querySelector("#SapXepTang");
const sapXepGiamBtn = document.querySelector("#SapXepGiam");
// LOCAL STORAGE
const NV_LOCALSTORAGE = "NHANVIENLIST";
// SET
function setLocalStorage(nhanVienList) {
  const nhanVienListJson = JSON.stringify(nhanVienList);
  localStorage.setItem(NV_LOCALSTORAGE, nhanVienListJson);
}
// GET
const getLocalStorage = () => {
  const nhanVienListJson = localStorage.getItem(NV_LOCALSTORAGE);
  if (nhanVienListJson != null) {
    nhanVienList = JSON.parse(nhanVienListJson);
    for (index = 0; index < nhanVienList.length; index++) {
      const nhanVien = nhanVienList[index];
      nhanVienList[index] = new NhanVien(
        nhanVien.accountId,
        nhanVien.name,
        nhanVien.email,
        nhanVien.password,
        nhanVien.startedDate,
        nhanVien.wages,
        nhanVien.position,
        nhanVien.workingHour
      );
    }
    renderNhanVienList(nhanVienList);
  }
};
getLocalStorage();

// RESET INPUT FORM
resetForm();
// ADD NHANVIEN EVENT //
addNvBtn.addEventListener("click", addNhanVien);
// UPDATE NHANVIEN EVENT //
updateNvBtn.addEventListener("click", UpdateNhanVien);
// CLOSE BUTTON EVENT //
closeBtn.addEventListener("click", resetForm);
// SEARCH BUTTON EVENT //
searchBtn.addEventListener("click", searchNhanVienByRank);
// CLEAR SEARCH EVENT //
clearSearchBtn.addEventListener("click", clearSearch);
// SAP XEP TANG EVENT
sapXepTangBtn.addEventListener("click", function () {
  sapXepTangBtn.classList.add("d-none");
  sapXepTangBtn.classList.remove("d-inline-block");
  sapXepGiamBtn.classList.add("d-inline-block");
  nhanVienList.sort(function (a, b) {
    return b.accountId * 1 - a.accountId * 1;
  });
  renderNhanVienList(nhanVienList);
});
// SAP XEP GIAM EVENT
sapXepGiamBtn.addEventListener("click", function () {
  sapXepGiamBtn.classList.add("d-none");
  sapXepGiamBtn.classList.remove("d-inline-block");
  sapXepTangBtn.classList.remove("d-none");
  sapXepTangBtn.classList.add("d-inline-block");

  nhanVienList.sort(function (a, b) {
    return `${a.accountId * 1}` - b.accountId * 1;
  });
  renderNhanVienList(nhanVienList);
});

function addNhanVien() {
  const nhanVien = getValuefromForm();
  const isValid = validateInput(nhanVien);
  const isNotDuplicated = checkDuplicatedAdd(nhanVien, nhanVienList);
  if (isValid && isNotDuplicated) {
    nhanVienList.push(nhanVien);
    setLocalStorage(nhanVienList);
    renderNhanVienList(nhanVienList);
    displayAlert("Thêm Nhân Viên Thành Công", "success", ".alert");
    $("#btnThemNV").attr("data-dismiss", "modal");
    resetForm();
  }
}

function UpdateNhanVien() {
  const nhanVien = getValuefromForm();
  const isValid = validateInput(nhanVien);
  const isNotDuplicated = checkDuplicatedUpdate(nhanVien, nhanVienList);
  if (isValid && isNotDuplicated) {
    nhanVienList[editIndex] = nhanVien;
    setLocalStorage(nhanVienList);
    renderNhanVienList(nhanVienList);
    displayAlert("Update thành công", "success", ".alert-modal");
  }
}

function deleteNhanVien(id) {
  const index = nhanVienList.findIndex((Element) => {
    return Element.accountId == id;
  });

  nhanVienList.splice(index, 1);
  setLocalStorage(nhanVienList);
  renderNhanVienList(nhanVienList);
  displayAlert("Đã Xóa Nhân Viên!", "danger", ".alert");
  resetForm();
}
function editNhanVien(id) {
  const index = nhanVienList.findIndex((Element) => {
    return Element.accountId == id;
  });

  if (index != -1) {
    const nhanVien = nhanVienList[index];
    editId = nhanVien.accountId;
    editIndex = index;
    showValueToForm(nhanVien);
    updateNvBtn.disabled = false;
    addNvBtn.disabled = true;
  }
}

function searchNhanVienByRank() {
  const searchName = document.querySelector("#searchName").value;
  let searchList = [];
  for (index = 0; index < nhanVienList.length; index++) {
    let nhanVien = nhanVienList[index];
    let rank = nhanVien.classifyNhanVien();
    if (searchName.toLowerCase() === rank.toLowerCase()) {
      searchList.push(nhanVien);
    }
  }
  if (searchList.length == 0) {
    displayAlert("Search Failed", "danger", ".alert");
  } else {
    displayAlert("Search Success", "success", ".alert");
  }
  renderNhanVienList(searchList);
}
function clearSearch() {
  document.querySelector("#searchName").value = "";
  renderNhanVienList(nhanVienList);
}

function resetForm() {
  document.querySelector("#tknv").readOnly = false;

  document.querySelector("#tknv").value = "";
  document.querySelector("#name").value = "";
  document.querySelector("#email").value = "";
  document.querySelector("#password").value = "";
  document.querySelector("#datepicker").value = "";
  document.querySelector("#luongCB").value = "";
  document.querySelector("#chucvu").value = "";
  document.querySelector("#gioLam").value = "";

  document.querySelector("#tbTKNV").innerText = "";
  document.querySelector("#tbTen").innerText = "";
  document.querySelector("#tbEmail").innerText = "";
  document.querySelector("#tbMatKhau").innerText = "";
  document.querySelector("#tbNgay").innerText = "";
  document.querySelector("#tbLuongCB").innerText = "";
  document.querySelector("#tbChucVu").innerText = "";
  document.querySelector("#tbGiolam").innerText = "";

  addNvBtn.disabled = false;
  updateNvBtn.disabled = true;
}
function displayAlert(text, action, target) {
  const alertElement = document.querySelector(`${target}`);
  alertElement.innerText = text;
  alertElement.classList.add(`alert-${action}`);
  alertElement.classList.remove(`collapse`);
  // remove alert
  setTimeout(function () {
    alertElement.innerText = "";
    alertElement.classList.remove(`alert-${action}`);
  }, 1000);
}
function showPassword() {
  let password = document.querySelector("#password");
  if (password.type === "password") {
    password.type = "text";
  } else {
    password.type = "password";
  }
}
